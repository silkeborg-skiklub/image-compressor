#!/usr/bin/env bash
# List all .jpg files
jpg=$(find . -name *.jpg)

function resize {
    echo Resizing $1
    # If height less than 1440
    if [ $(identify $1 | awk '{print $3}' | awk -F x '{print $2}') -lt 1440 ]
    then
        # resize to 720 px height if above 720 px
        convert $1 -resize 'x720>' $1
    else
        # resize to 1440 px height if above 1440 px
        convert $1 -resize 'x1440>' $1
    fi
}

#######
# JPG #
#######

echo -n 'total jpg size before script: '
du -ch $jpg | tail -1 | cut -f 1

for item in $jpg
do
    resize $item
    
    echo Compressing $item
    # Compress images, full explanation at:
    # https://stackoverflow.com/questions/7261855/recommendation-for-compressing-jpg-files-with-imagemagick
	convert $item -gaussian-blur 0.05 -sampling-factor 4:2:0 -define jpeg:dct-method=float -strip -quality 80 -interlace JPEG -colorspace RGB $item
done

echo -n 'total jpg size after script: '
du -ch $jpg | tail -1 | cut -f 1

#######
# PNG #
#######

# List all .png files
png=$(find . -name *.png)

echo -n 'total png size before script: '
du -ch $png | tail -1 | cut -f 1

for item in $png
do
    resize $item

    echo Optimizing $item
    # Optimize png
    optipng -silent -o5 -strip all $item
done

echo -n 'total png size after script: '
du -ch $png | tail -1 | cut -f 1
